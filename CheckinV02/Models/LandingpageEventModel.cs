﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckinV02.Models
{
    public class LandingpageEventModel:EventBaseModel
    {
        /// <summary>
        /// Định dạng SMS gửi của sự kiện
        /// </summary>
        public string SMSTitle { get; set; }
    }
}
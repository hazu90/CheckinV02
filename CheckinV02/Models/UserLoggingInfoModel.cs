﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckinV02.Models
{
    public class UserLoggingInfoModel
    {
        public string UserName { get; set; }
        public int Status { get; set; }
        public DateTime? LockedStartDate { get; set; }
        public int LockTime { get; set; }
        public List<int> Roles { get; set; }
        public int Role { get; set; }
        public string Password { get; set; }
    }
}
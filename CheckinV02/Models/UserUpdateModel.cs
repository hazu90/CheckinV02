﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CheckinV02.Common;

namespace CheckinV02.Models
{
    public class UserUpdateModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public List<int> LstRole { get; set; }
        public string StrRole { get; set; }
        public List<KeyValuePair<int, string>> AllRole { 
            get {
                return CheckInHelper.ListPermission();
            } 
        }
    }
}
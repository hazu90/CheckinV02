﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckinV02.Models
{
    public class DefaultEventModel
    {
        public int Id { get; set; }
        public string EventName { get; set; }
    }
}
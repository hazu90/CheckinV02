﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckinV02.Models
{
    public class InviteeIndexModel
    {
        public List<UserRoleModel> LstAssignTo { get; set; }
        public List<EventBaseModel> LstEvent { get; set; }
        public EventBaseModel DefaultEvent { get; set; }
        public bool IsExportExcelPermission { get; set; }
    }
}
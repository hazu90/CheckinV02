﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckinV02.Models
{
    public class UserViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public int Role { get; set; }
        public int Status { get; set; }
        public DateTime? LockedStartDate { get; set; }
        public int LockTime { get; set; }
        public string LockTimeHtml { 
            get{
                if(LockTime >0)
                {
                    var dayMod = LockTime % 1440;
                    var day = (LockTime - dayMod) / 1440;
                    var minute = dayMod % 60;
                    var hour = (dayMod - minute) / 60;
                    return string.Format("{0} Ngày {1} Giờ {2} Phút", day, hour, minute);
                }
                else
                {
                    return "";
                }
            }  
        }
        public List<string> LstRoleName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeactive { get; set; }
        public bool IsEditable { get; set; }
    }
}
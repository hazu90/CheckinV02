﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckinV02.Models
{
    public class UserAuthenticatedModel
    {
        public string UserName { get; set; }
        public List<int> Roles { get; set; }
        public bool HasRole(params int[] role )
        {
            foreach (var item in role)
            {
                if (Roles.Contains(item))
                {
                    return true;    
                }
            }
            return false;
        }
    }
}
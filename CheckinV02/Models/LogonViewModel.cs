﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckinV02.Models
{
    public class LogonViewModel
    {
        public string UserName { get; set; }
        public string Password{get;set;}
        public bool RememberMe { get; set; }
        public int Status { get; set; }
        public LogonViewModel()
        {
            UserName = "";
            Password = "";
            RememberMe = false;
            Status = 0;
        }
    }
}
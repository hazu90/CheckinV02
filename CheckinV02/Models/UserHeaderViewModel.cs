﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckinV02.Models
{
    public class UserHeaderViewModel
    {
        /// <summary>
        /// User Management
        /// </summary>
        public bool IsUserManager { get; set; }
        /// <summary>
        /// Invitee Management
        /// </summary>
        public bool IsInviteeManager { get; set; }
        /// <summary>
        /// Event Management
        /// </summary>
        public bool IsEventManager { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckinV02.Models
{
    public class GeneralEventModel:EventBaseModel
    {
        /// <summary>
        /// Ngày chốt sự kiện
        /// </summary>
        public DateTime Deadline { get; set; }
        /// <summary>
        /// Định dạng SMS gửi của sự kiện
        /// </summary>
        public string SMSTitle { get; set; }
        /// <summary>
        /// Áp dụng cho nhóm
        /// </summary>
        public string Deploy { get; set; }
        /// <summary>
        /// Ngày bắt đầu tính doanh thu
        /// </summary>
        public DateTime RevenueStartDate { get; set; }
        /// <summary>
        /// Ngày kết thúc tính doanh thu
        /// </summary>
        public DateTime RevenueEndDate { get; set; }
        /// <summary>
        /// Cách tính doanh thu
        /// </summary>
        public int RevenueCalculation { get; set; }
        /// <summary>
        /// Cho phép đính kèm hay không
        /// </summary>
        public bool IsAllowAttach { get; set; }
    }
}
﻿using CheckinV02.BL;
using CheckinV02.Common;
using CheckinV02.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CheckinV02.Controllers
{
    public class CommonController : Controller
    {
        ICurrUserBL CurrentUser;
        public CommonController(ICurrUserBL currentUser)
        {
            this.CurrentUser = currentUser;
        }

        // GET: Common
        public ActionResult Header()
        {
            var currUser = CurrentUser.GetCurrentUser();

            var userHeaderViewer = new UserHeaderViewModel();
            // Check user management permission
            if (currUser.HasRole(Permission.Admin.GetHashCode(), Permission.Manager.GetHashCode()))
            {
                userHeaderViewer.IsUserManager = true;
            }
            // Check invitee management permission
            if (currUser.HasRole(Permission.Admin.GetHashCode(), Permission.Manager.GetHashCode(), Permission.InviteeManager.GetHashCode()))
            {
                userHeaderViewer.IsInviteeManager = true;
            }
            // Check event management permission
            if (currUser.HasRole(Permission.Admin.GetHashCode(), Permission.Manager.GetHashCode()))
            {
                userHeaderViewer.IsEventManager = true;
            }

            return View(userHeaderViewer);
        }

        public ActionResult Paging(PagerModel pager)
        {
            return View(pager);
        }
    }
}
﻿using CheckinV02.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CheckinV02.Controllers
{
    public class BaseController : Controller
    {
        public UserAuthenticatedModel UserContext
        {
            get { return GetCurrentUser(); }
        }
        private UserAuthenticatedModel GetCurrentUser()
        {
            if (System.Web.HttpContext.Current == null) return null;
            var user = System.Web.HttpContext.Current.User;
            if (user != null && !string.IsNullOrWhiteSpace(user.Identity.Name))
            {
                if (!string.IsNullOrWhiteSpace(user.Identity.Name))
                {
                    return JsonConvert.DeserializeObject<UserAuthenticatedModel>(user.Identity.Name);
                }
            }
            return null;
        }
    }
}
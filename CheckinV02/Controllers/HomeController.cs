﻿using CheckinV02.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CheckinV02.Controllers
{
    public class HomeController : Controller
    {
        ICurrUserBL CurrentUser;
        public HomeController(ICurrUserBL currentUser)
        {
            this.CurrentUser = currentUser;
        }

        public ActionResult Index(bool justLogin = false)
        {
            var currUser = CurrentUser.GetCurrentUser();
            if (currUser != null)
            {
                return View();
            }
            return RedirectToAction("LogOn", "Account");
        }
    }
}
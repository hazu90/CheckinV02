﻿using CheckinV02.BL;
using CheckinV02.Common;
using CheckinV02.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CheckinV02.Controllers
{
    public class InviteeController : Controller
    {
        ICurrUserBL CurrentUser;
        IEventBL EventBL;
        IUserBL UserBL;
        public InviteeController(ICurrUserBL currentUser, IEventBL eventBL, IUserBL userBL)
        {
            this.CurrentUser = currentUser;
            this.EventBL = eventBL;
            this.UserBL = userBL;
        }
        public ActionResult Index()
        {
            var currUser = CurrentUser.GetCurrentUser();
            if(!currUser.HasRole(Permission.Admin.GetHashCode(),
                                Permission.InviteeManager.GetHashCode(),
                                Permission.Manager.GetHashCode() ) )
            {
                return View("~/Views/Common/NotPermitted.cshtml");
            }
            var model = new InviteeIndexModel();

            // Lấy thông tin sự kiện mặc định
            model.DefaultEvent = EventBL.GetDefault();
            // Lấy danh sách các CSKH có tài khoản trên hệ thống
            model.LstAssignTo = UserBL.GetByRole(Permission.InviteeManager.GetHashCode());
            // Lấy ra danh sách sự kiện
            model.LstEvent = EventBL.GetActiveAndOrderedCreatedDate();
            // Kiểm tra xem người dùng có quyền xuất excel hay không
            if(currUser.HasRole(Permission.Admin.GetHashCode(),
                                Permission.Manager.GetHashCode()))
            {
                model.IsExportExcelPermission = true;
            }

            return View(model);
        }
    }
}
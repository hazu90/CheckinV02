﻿using CheckinV02.BL;
using CheckinV02.Common;
using CheckinV02.Models;
using Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace CheckinV02.Controllers
{
    public class UserController : Controller
    {
        ICurrUserBL CurrentUser;
        IUserBL UserBL;
        public UserController(ICurrUserBL currentUser, IUserBL userBL)
        {
            this.CurrentUser = currentUser;
            this.UserBL = userBL;
        }
        public ActionResult Index()
        {
            var currUser = CurrentUser.GetCurrentUser();
            if (currUser.HasRole(Permission.Manager.GetHashCode(), Permission.Admin.GetHashCode()))
            {
                var model = new UserIndexModel();
                // Check add user permission
                if (currUser.HasRole(Permission.Admin.GetHashCode()))
                {
                    model.IsAddNewUser = true;
                }
                return View(model);
            }
            else
            {
                return View("~/Views/Common/ErrorManual.cshtml", null, "Bạn không có quyền thực hiện chức năng này");
            }
        }

        public ActionResult Search( UserIndexModel model)
        {
            Thread.Sleep(2000);
            var currUser = CurrentUser.GetCurrentUser();
            if (!currUser.HasRole(Permission.Admin.GetHashCode(), Permission.Manager.GetHashCode()))
            {
                return View("~/Views/Common/ErrorManual.cshtml", null, "Bạn không có quyền thực hiện chức năng này");
            }
            var totalRecord = 0;
            var lstUser = UserBL.Search(currUser, model,out totalRecord);
            var pager = new PagerModel { CurrentPage = model.PageIndex, PageSize = model.PageSize, TotalItem = totalRecord };
            ViewBag.Pager = pager;
            return View(lstUser);
        }

        public ActionResult AddNew()
        {
            Thread.Sleep(2000);
            var model = new UserUpdateModel();
            return View(model);
        }

        public JsonResult AddNewAction(UserUpdateModel model)
        {
            Thread.Sleep(2000);
            var response = new Response();
            var currUser = CurrentUser.GetCurrentUser();
            if(!currUser.HasRole(Permission.Admin.GetHashCode()))
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
            // Bổ sung thông tin người dùng mới
            response= UserBL.AddNewUser(model);
            if(response.Code == SystemCode.Success)
            {
                response.Message = "Bạn đã thêm mới tài khoản thành công !";
            }
            return Json(response, JsonRequestBehavior.DenyGet);
        }
    }
}
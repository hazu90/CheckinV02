﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using CheckinV02.Models;
using DVS.Algorithm;
using Libs;
using CheckinV02.BL;
using CheckinV02.Common;
using System.Web.Security;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CheckinV02.Controllers
{
    public class AccountController : Controller
    {
        IUserBL UserBusinessLayer ;
        IUserRoleBL UserRoleBusinessLayer;

        public AccountController(IUserBL userBusinessLayer,IUserRoleBL userRoleBusinessLayer)
        {
            this.UserBusinessLayer = userBusinessLayer;
            this.UserRoleBusinessLayer = userRoleBusinessLayer;
        }

        public ActionResult LogOn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogOn(string username, string password, string returnUrl)
        {
            //Get user with user and password user just entry
            var checkedUser = UserBusinessLayer.GetUserSecurity(username, password.ToMD5());
            if (checkedUser != null)
            {
                //If User deactive then return view 
                if (!checkedUser.Status.Equals(UserState.Active.ToInt(-1)))
                {
                    return View("~/Views/Common/ErrorManual.cshtml", null, "Tài khoản Admin  của bạn đã bị khóa. Vui lòng đăng nhập tài khoản khác hoặc ấn vào <a href='/Account/LogOff'>đây</a> để đăng nhập lại!");
                }
                else
                {
                    if (checkedUser.LockedStartDate != null)
                    {
                        var dateLock = checkedUser.LockedStartDate.Value.AddMinutes(checkedUser.LockTime);
                        if (dateLock >= DateTime.Now)
                        {
                            return View("~/Views/Common/ErrorManual.cshtml", null, "Tài khoản của bạn đã bị khóa. Vui lòng đăng nhập tài khoản khác hoặc ấn vào <a href='/Account/LogOff'>đây</a> để đăng nhập lại!");
                        }
                    }
                }

                // Lấy danh sách các role của người dùng vừa đăng nhập
                var lstRoles = UserRoleBusinessLayer.GetByUserName(checkedUser.UserName);
                checkedUser.Roles = new List<int>();
                foreach (var roleInfo in lstRoles)
                {
                    checkedUser.Roles.Add(roleInfo.RoleId);
                }
                FormsAuthentication.SetAuthCookie(JsonConvert.SerializeObject(checkedUser, Formatting.None), false);
                if (!string.IsNullOrEmpty(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                var response = new LogonViewModel
                {
                    UserName = username,
                    Password = password,
                    Status = 1
                };
                return View(response);
            }
        }
    }
}
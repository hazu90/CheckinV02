﻿using CheckinV02.BL;
using CheckinV02.DAL;
using Ninject;
using Ninject.Web.Common.WebHost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace CheckinV02
{
    public class MvcApplication : NinjectHttpApplication
    {
        //protected void Application_Start()
        //{
        //    AreaRegistration.RegisterAllAreas();
        //    FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
        //    RouteConfig.RegisterRoutes(RouteTable.Routes);
        //    BundleConfig.RegisterBundles(BundleTable.Bundles);
        //}

        protected override Ninject.IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            RegisterServices(kernel);
            return kernel;
        }
        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private void RegisterServices(IKernel kernel)
        {
            // DAL
            kernel.Bind<IUserRespository>().To<UserRespository>().InTransientScope();
            kernel.Bind<IUserRoleRespository>().To<UserRoleRespository>().InTransientScope();
            kernel.Bind<IEventRespository>().To<EventRespository>().InTransientScope();
            // BL
            kernel.Bind<IUserBL>().To<UserBL>().InTransientScope();
            kernel.Bind<IUserRoleBL>().To<UserRoleBL>().InTransientScope();
            kernel.Bind<ICurrUserBL>().To<CurrUserBL>().InTransientScope();
            kernel.Bind<IEventBL>().To<EventBL>().InTransientScope();
        }
        protected override void OnApplicationStarted()
        {
            base.OnApplicationStarted();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}

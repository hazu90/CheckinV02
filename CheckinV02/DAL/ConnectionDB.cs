﻿using FluentData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace CheckinV02.DAL
{
    public class ConnectionDB
    {
        public static IDbContext MainDB()
        {
            return new DbContext().ConnectionString(ConfigurationManager.AppSettings["DVSCheckIn"], new SqlServerProvider());
        }
        public static IDbContext BCRMv3()
        {
            return new DbContext().ConnectionString(ConfigurationManager.AppSettings["BCRMv3"], new SqlServerProvider());
        }
    }
}
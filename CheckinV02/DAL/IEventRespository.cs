﻿using CheckinV02.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckinV02.DAL
{
    public interface IEventRespository
    {
        List<EventBaseModel> GetAll();
    }
}

﻿using CheckinV02.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckinV02.DAL
{
    public class SetDefaultEventRespository:ISetDefaultEventRespository
    {
        public SetDefaultEventModel GetLastest()
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("SetDefaultEvent_GetLastest")
                                .QuerySingle<SetDefaultEventModel>();
            }
        }
    }
}
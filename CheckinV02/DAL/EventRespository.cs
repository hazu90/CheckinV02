﻿using CheckinV02.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckinV02.DAL
{
    public class EventRespository : IEventRespository
    {
        public List<EventBaseModel> GetAll()
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("Event_GetAll")
                                .QueryMany<EventBaseModel>();
            }
        }
    }
}
﻿using CheckinV02.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckinV02.DAL
{
    public interface IUserRoleRespository
    {
        List<UserRoleModel> GetByUserName(string userName);
        bool Insert(UserRoleModel model);
    }
}

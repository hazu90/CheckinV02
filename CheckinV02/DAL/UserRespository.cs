﻿using CheckinV02.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckinV02.DAL
{
    public class UserRespository:IUserRespository
    {
        public UserLoggingInfoModel GetUserSecurity(string userName, string password)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("User_GetByUserNamePassword")
                              .Parameter("UserName", userName)
                              .Parameter("Password", password)
                              .QuerySingle<UserLoggingInfoModel>();
            }
        }
        public List<UserViewModel> GetList(UserIndexModel model,out int totalRecord)
        {
            using (var context = ConnectionDB.MainDB())
            {
                var cmd = context.StoredProcedure("User_GetList")
                              .Parameter("UserName", model.UserName)
                              .Parameter("PageIndex", model.PageIndex)
                              .Parameter("PageSize", model.PageSize)
                              .ParameterOut("TotalRecord", FluentData.DataTypes.Int32);
                var result = cmd.QueryMany<UserViewModel>();
                totalRecord = cmd.ParameterValue<int>("TotalRecord");
                return result;
            }
        }
        public bool Insert(UserUpdateModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("User_Insert")
                              .Parameter("UserName", model.UserName)
                              .Parameter("PassWord", model.Password)
                              .Parameter("Role", 0)
                              .QuerySingle<int>() > 0;
            }
        }
        public List<UserRoleModel> GetByRole(int role)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("User_GetByRole")
                              .Parameter("Role", role)
                                .QueryMany<UserRoleModel>();
            }
        }
    }
}
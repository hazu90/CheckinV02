﻿using CheckinV02.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckinV02.DAL
{
    public interface IUserRespository
    {
        UserLoggingInfoModel GetUserSecurity(string userName, string password);
        List<UserViewModel> GetList(UserIndexModel model, out int totalRecord);
        List<UserRoleModel> GetByRole(int role);
        bool Insert(UserUpdateModel model);
    }
}

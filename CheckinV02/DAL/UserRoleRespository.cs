﻿using CheckinV02.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckinV02.DAL
{
    public class UserRoleRespository : IUserRoleRespository
    {
        public List<UserRoleModel> GetByUserName(string userName)
        {
            using(var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("UserRole_GetByUserName")
                                .Parameter("UserName", userName)
                                .QueryMany<UserRoleModel>();
            }
        }
        public bool Insert(UserRoleModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("UserRole_Insert")
                                .Parameter("UserName", model.UserName)
                                .Parameter("Role", model.RoleId)
                                .QuerySingle<int>() > 0;
            }
        }
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CheckinV02.Startup))]
namespace CheckinV02
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

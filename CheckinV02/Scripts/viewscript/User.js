var user_index = {
    get_form_search: function () {
       return $('#frmSearchUser');
    },
    get_field_val: function (field_name,defaultVl ) {
        var frm = user_index.get_form_search();
        if(field_name=='SearchText'){
            return $('#txtSearchText',frm).val();
        }
        else{
            return '';
        }

    },
    init: function () {
        $(document).ready(function () {
            var frm_search = user_index.get_form_search();
            user_index.action();
        });
    },
    action: function () {
        var frm_search = user_index.get_form_search();
        $('#btnSearch',frm_search).off('click').on('click', function () {
            user_index.search(1);
        });
        $("#btnAddNew", frm_search).off('click').on('click', function () {
            user_index.display('AddNewUser');
            var callback_btn_addnew = function (response) {
                $(".panel-body", $("#AddNewUser")).html(response);
                user_addnew.init();
            };
            sys_call_ajax.call_html_type('User','AddNew',{},$("#AddNewUser"),callback_btn_addnew);
        });
    },
    action_grid: function () {

    },
    search: function (page) {
        user_index.display('SearchUser');
        var frm_search = user_index.get_form_search();
        if (page == null || page == '') {
            page = 1;
        }
        var userName = user_index.get_field_val('SearchText','');
        var model = {
            UserName: userName,
            PageIndex: page,
            PageSize: 25
        };
        var callback_search = function (response) {
            $(".panel-body", $("#SearchUser")).html(response);
        };
        sys_call_ajax.call_html_type('User','Search',model,$('#SearchUser'),callback_search);
    },
    display: function (div_elm) {
        var arr_Div = ['AddNewUser','SearchUser'];
        for(var index =0;index<arr_Div.length;index++){
            if(div_elm == arr_Div[index]){
                $('#'+arr_Div[index]).removeClass('hide');
            }
            else{
                $('#'+arr_Div[index]).addClass('hide');
            }
        }
    }
};
user_addnew={
    init: function () {
        user_addnew.action();
    },
    action: function () {
        var frm_addnew = $("#frmAddNewUser");
        $("#btnSave",frm_addnew).off("click").on('click', function () {
            sys_utility.clear_errors($("#frmAddNewUser"));
            if(user_addnew.validate()){
                var userName = sys_control.get_val_to_ajax($("#txtUsername", frm_addnew));
                var password = sys_control.get_val_to_ajax($("#txtPassword", frm_addnew));
                var cfmPassword = sys_control.get_val_to_ajax($("#txtReTypePassword", frm_addnew));
                var strRole = sys_control.get_val_to_ajax_bygroup_checkbox($('input:checkbox[name=grRole]'),'renum');
                var model = {
                    UserName: userName,
                    PassWord: password,
                    StrRole:strRole
                };
                var callback_insert_user = function (response) {
                    if(response.Code == ResponseCode.Success){
                        sysmess.success(response.Message);
                    }
                    else if(response.Code == ResponseCode.Error){
                        sysmess.error(response.Message);
                    }
                    else{
                        sysmess.warning(response.Message);
                    }
                };
                sys_call_ajax.call_json_type('User','AddNewAction',model,$('#AddNewUser'),callback_insert_user);
            }
        });
        $('#btnCancel',frm_addnew).off('click').on('click', function () {
            sys_utility.clear_errors(frm_addnew);
            sys_utility.clear_all_controls(frm_addnew);
        });
        $('#btnClose',frm_addnew).off('click').on('click', function () {
            user_index.display('SearchUser');
        });
    },
    validate: function () {
        var frm_addnew = $("#frmAddNewUser");
        var is_valid = true;
        if(!sys_utility.require_text_box($("#txtUsername", frm_addnew))){
            sys_control.show_msg($("#txtUsername", frm_addnew),'Tên tài khoản bắt buộc nhập !',1);
            is_valid = false;
        }
        if(!sys_utility.require_text_box($("#txtPassword", frm_addnew))){
            sys_control.show_msg($("#txtPassword", frm_addnew),'Mật khẩu bắt buộc nhập !',1);
            is_valid = false;
        }
        if(!sys_utility.require_text_box($("#txtReTypePassword", frm_addnew))){
            sys_control.show_msg($("#txtReTypePassword", frm_addnew),'Nhập lại mật khẩu bắt buộc nhập !',1);
            is_valid = false;
        }
        if(is_valid){
            if(!sys_utility.compare_text($("#txtPassword", frm_addnew).val(),$("#txtReTypePassword", frm_addnew).val() )){
                sys_control.show_msg($("#txtReTypePassword", frm_addnew),'Thông tin xác nhân mật khẩu chưa đúng !',1);
                is_valid = false;
            }
        }
        if(!sys_utility.is_choose_checkbox_group($('input:checkbox[name=grRole]'),'renum')){
            sys_control.show_msg($("input:checkbox[name=grRole]:first-child", frm_addnew),'Bạn phải thiết lập quyền cho tài khoản này !',1);
            is_valid = false;
        }
        return is_valid;
    }
}
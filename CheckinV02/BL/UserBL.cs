﻿using CheckinV02.Common;
using CheckinV02.DAL;
using CheckinV02.Models;
using Libs;
using DVS.Algorithm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckinV02.BL
{
    public class UserBL:IUserBL
    {
        IUserRespository UserRepository;
        IUserRoleRespository UserRoleRepository;
        public UserBL(IUserRespository UserRepository, IUserRoleRespository UserRoleRepository)
        {
            this.UserRepository = UserRepository;
            this.UserRoleRepository = UserRoleRepository;
        }
        public UserLoggingInfoModel GetUserSecurity(string userName, string password)
        {
            return UserRepository.GetUserSecurity(userName, password);
        }
        public List<UserViewModel> Search(UserAuthenticatedModel currUser, UserIndexModel model, out int totalRecord)
        {
            var lstUser = UserRepository.GetList(model, out totalRecord);
            var isAdminPermission = false;
            if (currUser.HasRole(Permission.Admin.GetHashCode()))
            {
                isAdminPermission = true;
            }

            foreach (var userInfo in lstUser)
            {
                var lstRoles = UserRoleRepository.GetByUserName(userInfo.UserName);
                userInfo.LstRoleName = lstRoles.Select(o => CheckInHelper.ToPermissionName((Permission)o.RoleId)).ToList();
                if(isAdminPermission)
                {
                    if(userInfo.Status == 1)
                    {
                        userInfo.IsDeactive = true;
                    }
                    else
                    {
                        userInfo.IsActive = true;
                    }
                    userInfo.IsEditable = true;
                }
            }
            return lstUser;
        }
        public Response AddNewUser(UserUpdateModel model)
        {
            var response = new Response();
            if (string.IsNullOrEmpty(model.UserName) || string.IsNullOrEmpty(model.Password))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Dữ liệu nhập vào không đúng";
                return response;
            }
            // Convert password sang MD5
            model.Password = model.Password.ToMD5();
            var result= UserRepository.Insert(model);
            if(result)
            {
                // Khai báo role mới cho nhân viên đó
                var lstRole = model.StrRole.Split(',').ToList();
                foreach (var roleItem in lstRole)
                {
                    UserRoleRepository.Insert(new UserRoleModel()
                    {
                        RoleId = int.Parse(roleItem),
                        UserName = model.UserName
                    });
                }
            }
            return response;
        }
        public List<UserRoleModel> GetByRole(int role)
        {
            return UserRepository.GetByRole(role);
        }
    }
}
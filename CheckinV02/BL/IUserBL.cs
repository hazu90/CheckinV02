﻿using CheckinV02.Models;
using Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckinV02.BL
{
    public interface IUserBL
    {
        UserLoggingInfoModel GetUserSecurity(string userName, string password);
        List<UserViewModel> Search(UserAuthenticatedModel currUser, UserIndexModel model,out int totalRecord);
        Response AddNewUser(UserUpdateModel model);
        List<UserRoleModel> GetByRole(int role);
    }
}

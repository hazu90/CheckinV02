﻿using CheckinV02.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckinV02.BL
{
    public interface IUserRoleBL
    {
        List<UserRoleModel> GetByUserName(string userName);
    }
}

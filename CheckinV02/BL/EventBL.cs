﻿using CheckinV02.DAL;
using CheckinV02.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckinV02.BL
{
    public class EventBL : IEventBL
    {
        IEventRespository EventRespository;
        ISetDefaultEventRespository SetDefaultEventRespository;
        public EventBL(IEventRespository eventRespository, ISetDefaultEventRespository setDefaultEventRespository)
        {
            this.EventRespository = eventRespository;
            this.SetDefaultEventRespository = setDefaultEventRespository;
        }
        /// <summary>
        /// Lấy ra thông tin sự kiện được set mặc định hoặc sự kiện mới nhất
        /// </summary>
        /// <returns>
        /// Thông tin của sự kiện được set mặc định hoặc sự kiện mới nhất
        /// </returns>
        public EventBaseModel GetDefault()
        {
            // Lấy danh sách các sự kiện đang hoạt động
            var lstAll = EventRespository.GetAll().FindAll(o=>o.IsEnable == true);
            if (lstAll.Count == 0)
            {
                return null;
            }
            // Lấy danh sách sự kiện được set mặc định
            var defaultEvent = SetDefaultEventRespository.GetLastest();
            // TH chưa có thông tin về sự kiện được đặt mặc định
            // Lấy ra sự kiện mới nhất
            var lastestEvent = new EventBaseModel();
            if(defaultEvent == null)
            {
                lastestEvent = lstAll.OrderByDescending(o => o.CreatedDate).ToList().FirstOrDefault();
            }
            else
            {
                if (defaultEvent.IsCancel)
                {
                    return null;
                }
                lastestEvent = lstAll.Find(o => o.Id == defaultEvent.EventId );
                if(lastestEvent == null)
                {
                    lastestEvent = lstAll.OrderByDescending(o => o.CreatedDate).ToList().FirstOrDefault();
                }
            }
            return lastestEvent;
        }
        public List<EventBaseModel> GetActiveAndOrderedCreatedDate()
        {
            return EventRespository.GetAll().FindAll(o => o.IsEnable == true).OrderByDescending(o => o.CreatedDate).ToList();
        }
    }
}
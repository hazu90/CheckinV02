﻿using CheckinV02.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckinV02.BL
{
    public interface ICurrUserBL
    {
        UserAuthenticatedModel GetCurrentUser();
    }
}

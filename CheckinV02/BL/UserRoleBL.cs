﻿using CheckinV02.DAL;
using CheckinV02.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckinV02.BL
{
    public class UserRoleBL : IUserRoleBL
    {
        IUserRoleRespository UserRoleDL;
        public UserRoleBL(IUserRoleRespository userRoleDL)
        {
            this.UserRoleDL = userRoleDL;
        }
        public List<UserRoleModel> GetByUserName(string userName)
        {
            return UserRoleDL.GetByUserName(userName);
        }
    }
}
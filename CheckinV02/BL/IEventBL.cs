﻿using CheckinV02.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckinV02.BL
{
    public interface IEventBL
    {
        /// <summary>
        /// Lấy ra sự kiện mặc định hoặc là sự kiện được tạo mới nhất
        /// </summary>
        /// <returns>
        /// Thông tin sự kiện
        /// </returns>
        EventBaseModel GetDefault();
        /// <summary>
        /// Lấy ra danh sách các sự kiện đang hoạt động và có sắp xếp theo ngày tạo sự kiện
        /// </summary>
        /// <returns>
        /// Danh sách các sự kiện
        /// </returns>
        List<EventBaseModel> GetActiveAndOrderedCreatedDate();
    }
}

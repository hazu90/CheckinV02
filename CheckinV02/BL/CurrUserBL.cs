﻿using CheckinV02.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheckinV02.BL
{
    public class CurrUserBL:ICurrUserBL
    {
        public UserAuthenticatedModel GetCurrentUser()
        {
            if (System.Web.HttpContext.Current == null)
            {
                return null;
            }
            var user = System.Web.HttpContext.Current.User;
            if (user != null && !string.IsNullOrWhiteSpace(user.Identity.Name))
            {
                return JsonConvert.DeserializeObject<UserAuthenticatedModel>(user.Identity.Name);
            }
            return null;
        }
    }
}